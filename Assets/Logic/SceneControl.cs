﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SceneControl : MonoBehaviour
{
    public Action UpdateBackground = delegate { };
    public Action<List<AchievmentRawData>> UpdateAchievments = delegate { };

    static public SceneControl Instance
    {
        get
        {
            if (_instance == null)
                _instance = new GameObject("SceneControl").AddComponent<SceneControl>();
            return _instance;
        }
    }

    static private SceneControl _instance;

    [SerializeField]
    private int m_achievmentsCount = 100;
    private List<AchievmentRawData> m_achievments = new List<AchievmentRawData>();

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Debug.LogError($"Another instance of {nameof(SceneControl)} already exist, this one will be destroyed.");
            DestroyImmediate(this);
        }
    }

    private void Start()
    {
        GenerateAchievmentsData();
        UpdateAchievments?.Invoke(m_achievments);
    }

    /// <summary>
    /// Нагенерить значений для списка достижений
    /// </summary>
    private void GenerateAchievmentsData()
    {
        if (m_achievmentsCount < 1)
            throw new Exception($"Количество должно быть больше 0, задано: {m_achievmentsCount}");

        m_achievments.Clear();
        for (int i = 0; i < m_achievmentsCount; i++)
        {
            var grade = UnityEngine.Random.Range(0, 4);
            m_achievments.Add(new AchievmentRawData() { Counter = i, Grade = grade });
        }
    }
}
