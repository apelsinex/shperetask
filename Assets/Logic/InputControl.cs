﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace InputSystem
{
    public class MouseArgs
    {
        public Vector2 MousePosition { get; internal set; } = Vector2.zero;
        public Vector2 MousePositionPrevious { get; internal set; } = Vector2.zero;
        public Vector2 MousePositionDelta { get; internal set; } = Vector2.zero;
    }

    public class InputControl : MonoBehaviour
    {
        public Action<MouseArgs> MouseArgsUpdated = delegate { };

        static public InputControl Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new GameObject("InputControl").AddComponent<InputControl>();
                return _instance;
            }
        }

        static private InputControl _instance;

        private MouseArgs m_mouseArgs = new MouseArgs();

        public Vector2 MousePosition { get { return m_mouseArgs.MousePosition; } }
        public Vector2 MousePositionPrevious { get { return m_mouseArgs.MousePositionPrevious; } }
        public Vector2 MousePositionDelta { get { return m_mouseArgs.MousePositionDelta; } }

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else
            {
                Debug.LogError($"Another instance of {nameof(InputControl)} already exist, this one will be destroyed.");
                DestroyImmediate(this);
            }
        }

        private void Update()
        {
            m_mouseArgs.MousePositionPrevious = MousePosition;
            m_mouseArgs.MousePosition = Input.mousePosition;
            m_mouseArgs.MousePositionDelta = MousePosition - MousePositionPrevious;

            if (MousePositionDelta.magnitude > 0f)
                MouseArgsUpdated?.Invoke(m_mouseArgs);
        }
    }
}

