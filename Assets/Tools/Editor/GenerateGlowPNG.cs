﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;


public class GenerateGlowPNG : MonoBehaviour
{
    [MenuItem("Custom/Generate Glow PNG", false, 1)]
    static void GenerateCircleGlowPng()
    {
        var size = 128;
        Texture2D texture = new Texture2D(size, size, TextureFormat.ARGB32, false);

        for (int y = 0; y < texture.height; y++)
        {
            for (int x = 0; x < texture.width; x++)
            {
                Color color = Color.clear;

                var fromCenterCoords = new Vector2(2 * x - size, 2 * y - size) / size;
                var radius = Mathf.Sqrt(fromCenterCoords.x * fromCenterCoords.x + fromCenterCoords.y * fromCenterCoords.y);                

                var radialLengthNormalized = Mathf.Acos(fromCenterCoords.y / radius) / (2f * Mathf.PI);
                if (fromCenterCoords.x < 0)
                    radialLengthNormalized = 1f - radialLengthNormalized;

                // точноти несжатой текстуры 64х64 хватает, кодирование не требуется
                //var encodedLength = EncodeFloatRG(radialLengthNormalized);

                // red      - канал для расстояния по окружности, от верхней точки на 12 часов по часовой стрелке, нормализованное от 0 до 1
                // blue     - канал для базового круга
                // alpha    - канал для сияния вокруг базового круга
                color.r = radialLengthNormalized;
                color.b = radius < 0.85f ? 1f : radius > 0.88f ? 0f : (0.88f - radius) / 0.03f;
                color.a = radius < 0.85f ? 1f : radius > 1f ? 0f : (1f - radius) / 0.15f;

                texture.SetPixel(x, y, color);
            }
        }

        texture.Apply();

        byte[] bytes = texture.EncodeToPNG();

        var dirPath = Application.dataPath + "/_Debug/GeneratedImages/";
        if (!Directory.Exists(dirPath))
            Directory.CreateDirectory(dirPath);

        var path = dirPath + "Sphere1.png";
        File.WriteAllBytes(path, bytes);

        Debug.Log($"generated: {path}");
    }

    static Vector2 EncodeFloatRG(float v)
    {
        var kEncodeMul = new Vector2(1f, 255f);
        float kEncodeBit = 1f / 255f;
        var enc = kEncodeMul * v;
        enc.x = enc.x % 1;
        enc.y = enc.y % 1;
        enc.x -= enc.y * kEncodeBit;
        return enc;
    }

    static float DecodeFloatRG(Vector2 enc)
    {
        var kDecodeDot = new Vector2(1f, 1f / 255f);
        return Vector3.Dot(enc, kDecodeDot);
    }
}
