﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public interface IItemSettings { }

public interface IAdvancedScrollItem<T>
{
    void SetView(T data, IItemSettings settings);
}

[RequireComponent(typeof(ScrollRect))]
public class AdvancedScrollViewControl : MonoBehaviour
{
    [SerializeField]
    private GameObject m_itemPrefab = default;
    [SerializeField]
    private GameObject m_itemSettings = default;
    [SerializeField]
    private RectTransform m_content = default;
    //[SerializeField]
    //private RectTransform m_poolStorage = default;
    //[SerializeField]
    //private RectTransform m_spacerTop = default;
    //[SerializeField]
    //private RectTransform m_spacerBottom = default;

    private ScrollRect m_scrollRect = default;
    private SceneControl m_sceneControl = default;
    private bool m_isInitiated = false;

    public void Initiate()
    {
        if (m_isInitiated)
            return;

        m_sceneControl = SceneControl.Instance;
        m_sceneControl.UpdateAchievments += GenerateItems;
        m_scrollRect = GetComponent<ScrollRect>();

        m_isInitiated = true;
    }

    private void GenerateItems<T>(List<T> dataList)
    {
        if (!m_isInitiated)
            Initiate();

        var itemInterface = m_itemPrefab.GetComponent<IAdvancedScrollItem<T>>();
        if (itemInterface == null)
            throw new Exception($"Interface {typeof(IAdvancedScrollItem<T>)} not found in prefab: {m_itemPrefab.name}.");

        var settingsInterface = m_itemSettings.GetComponent<IItemSettings>();
        if (settingsInterface == null)
            throw new Exception($"Interface {typeof(IItemSettings)} not found in object: {m_itemSettings.name}.");

        for (int i = 0; i < dataList.Count; i++)
        {
            var gobj = GameObject.Instantiate(m_itemPrefab, m_content);

            var item = gobj.GetComponent<IAdvancedScrollItem<T>>();
            item.SetView(dataList[i], settingsInterface);
        }
    }

    private void OnScroll(Vector2 delta)
    {
        // TODO: apply pooling and update spacers here...
        //Debug.Log($"scolling with: {delta.ToString("F2")}");
    }

    private void OnEnable()
    {
        if (!m_isInitiated)
            Initiate();

        m_scrollRect.onValueChanged.AddListener(OnScroll);
    }

    private void OnDisable()
    {
        m_scrollRect.onValueChanged.RemoveListener(OnScroll);
    }
}
