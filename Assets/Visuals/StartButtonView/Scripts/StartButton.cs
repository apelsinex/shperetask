﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class StartButton : MonoBehaviour
{
    [SerializeField]
    private Material m_material = default;
    [SerializeField]
    private float m_animSpeed = 1f;

    private SceneControl m_sceneControl = default;
    private Button m_button = default;
    private string m_shaderCutProp = "_RadialCut";
    private float m_shaderCutFrom = 0f;
    private float m_shaderCutTo = 0f;
    private float m_shaderCutCurrent = 0f;
    private bool m_InShaderGlowAnim = false;

    private void Awake()
    {
        m_button = GetComponent<Button>();
    }

    private void Start()
    {
        m_sceneControl = SceneControl.Instance;
        m_sceneControl.UpdateBackground += OnClickedReceiver;
        if (m_material == null)
            m_material = ((Image)m_button?.targetGraphic)?.material;
        m_material.SetFloat(m_shaderCutProp, 1.51f);
    }

    private void OnEnable()
    {
        m_button.onClick.AddListener(ClickedReport);
    }

    private void OnDisable()
    {
        m_button.onClick.RemoveListener(ClickedReport);
    }

    private void ClickedReport()
    {
        m_sceneControl.UpdateBackground?.Invoke();
    }

    private void OnClickedReceiver()
    {
        RestartGlowAnimation();
    }

    private void RestartGlowAnimation()
    {
        m_shaderCutFrom = m_shaderCutCurrent = 0f;
        m_shaderCutTo = 1.51f;
        m_material.SetFloat(m_shaderCutProp, m_shaderCutCurrent);
        m_InShaderGlowAnim = true;
    }

    private void Update()
    {
        if (m_InShaderGlowAnim)
        {
            m_shaderCutCurrent += m_shaderCutFrom + Time.deltaTime * m_animSpeed;
            m_material.SetFloat(m_shaderCutProp, m_shaderCutCurrent);

            if (Mathf.Abs(m_shaderCutTo - m_shaderCutCurrent) < 0.001f)
                m_InShaderGlowAnim = false;
        }
    }
}
