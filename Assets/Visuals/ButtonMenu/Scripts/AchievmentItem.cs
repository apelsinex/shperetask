﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class AchievmentRawData
{
    public int Counter = 0;
    public int Grade = 0;
}

public class AchievmentViewData
{
    public string Title = "";
    public Sprite Sprite = default;
}

public class AchievmentItem : MonoBehaviour, IAdvancedScrollItem<AchievmentRawData>
{
    [SerializeField]
    private TextMeshProUGUI m_title = default;
    [SerializeField]
    private Image m_gradeImage = default;

    void IAdvancedScrollItem<AchievmentRawData>.SetView(AchievmentRawData rawData, IItemSettings settings)
    {
        if (rawData == null)
            throw new ArgumentException("Parameter cannot be null", nameof(rawData));
        if (settings == null)
            throw new ArgumentException("Parameter cannot be null", nameof(settings));

        var viewData = ((AchievmentItemSettings)settings).GetViewData(rawData);

        m_title.text = viewData.Title;
        m_gradeImage.sprite = viewData.Sprite;
    }
}
