﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievmentItemSettings : MonoBehaviour, IItemSettings
{
    [SerializeField]
    private List<Sprite> m_gradeSprites = new List<Sprite>();

    private string m_titleBase = "Достижение";

    public AchievmentViewData GetViewData(AchievmentRawData rawData)
    {
        var viewData = new AchievmentViewData()
        {
            Title = $"{m_titleBase} {rawData.Counter}",
            Sprite = m_gradeSprites[rawData.Grade]
        };
        return viewData;
    }
}
