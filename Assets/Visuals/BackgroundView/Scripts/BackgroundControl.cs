﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InputSystem;

public class BackgroundControl : MonoBehaviour
{
    [SerializeField]
    private Color[] m_backColors = new Color[3];
    [SerializeField]
    private Image m_backImage = default;
    [SerializeField]
    private Transform m_parallaxLayerClose = default;
    [SerializeField]
    private Transform m_parallaxLayerDistant = default;
    [SerializeField]
    private int m_maxShiftClose = 10;
    [SerializeField]
    private int m_maxShiftDistant = 6;

    private SceneControl m_sceneControl = default;
    private InputControl m_inputControl = default;
    private int m_backColorId = 0;
    private int m_screenWidth = 1;
    private Vector3 m_locPosClose = Vector3.zero;
    private Vector3 m_locPosDistant = Vector3.zero;

    private void Start()
    {
        m_sceneControl = SceneControl.Instance;
        m_inputControl = InputControl.Instance;

        m_sceneControl.UpdateBackground += SetNextBackColor;
        m_inputControl.MouseArgsUpdated += UpdateParallaxLayers;

        m_locPosClose = m_parallaxLayerClose.localPosition;
        m_locPosDistant = m_parallaxLayerDistant.localPosition;

        m_screenWidth = Screen.width;
        m_backImage.color = m_backColors[m_backColorId];
    }

    private void SetNextBackColor()
    {
        m_backColorId++;
        m_backColorId %= m_backColors.Length;
        m_backImage.color = m_backColors[m_backColorId];
    }

    private void UpdateParallaxLayers(MouseArgs args)
    {
        var relativeX = 2f * (m_screenWidth * 0.5f - args.MousePosition.x) / m_screenWidth;
        m_locPosClose.x = relativeX * m_maxShiftClose;
        m_locPosDistant.x = relativeX * m_maxShiftDistant;

        m_parallaxLayerClose.localPosition = m_locPosClose;
        m_parallaxLayerDistant.localPosition = m_locPosDistant;
    }
}
